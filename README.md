## Large Marge

Yet another slack bot. Have you ever dreamed about being nagged, ehm, reminded
about stale PRs in your project. Fear not stranger, ask Large Marge for help.

[![pipeline status](https://gitlab.com/the_speedball/large_marge/badges/master/pipeline.svg)](https://gitlab.com/the_speedball/large_marge/commits/master)


## Features

Each day at predefined time Large Marge will post a list of outstanding PRs, that
have not been updated for more than 24 hours.
You may ask Large Marge for help by saying "@large_marge help".

## Issues and requests

Requests and issues are tracked on projects website https://gitlab.com/the_speedball/large_marge/issues
If you wish you may as well send an email to incoming+the_speedball/large_marge@gitlab.com.

## Contributing

### Run 

### Test
