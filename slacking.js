const rollbar = require('rollbar');
const WebClient = require('@slack/client').WebClient;

let getUsersSlackId = (email, token) => {
    rollbar.debug(`Looking up slackID for ${email}`);
    const web = new WebClient(token);
    return new Promise((resolve, reject) => {
        web.users.list((err, rsp) => {
            if (err) {
                rollbar.error(err);
            }
            // FIXME: this needs to work with cursor in case user id is not found
            for (let user of rsp.members) {
                if (user.profile.email === email) {
                    rollbar.debug(`Found user.id ${user.id}`);
                    resolve(`<@${user.id}>`);
                }
            }
            // TODO:
            // if ID not found, return at least email
            // possibly remove the domain as some people would not like
            // to share their email publicly
            resolve(email);
            rollbar.debug('User not found. Fallback to email.');
        });
    });
};

exports.getUsersSlackId = getUsersSlackId;
