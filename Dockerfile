FROM node:9.2

WORKDIR /usr/src/app

COPY package.json .

RUN yarn
COPY . .


CMD ["npm", "start"]
