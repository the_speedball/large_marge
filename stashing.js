const rollbar = require('rollbar');
const request = require('request');
const printf = require('util').format;
const stashUrl = 'https://stash.clearcode.cc/rest/api/1.0/projects/%s/repos/%s/pull-requests'; // projectSlug, repoSlug
const moment = require('moment');


let fetchPrs = () => {
    return new Promise((resolve, reject) => {
        request.get(
            // FIXME: rotate over list of repos
            // this should grab a list of repos under project
            // possibly move out into main. At least part that specifies what to pull
            printf(stashUrl, 'CCADS', 'backend'), {
                auth: {
                    user: process.env.stash_user,
                    pass: process.env.stash_pass
                }
            },
            (error, response, body) => {
                if (error) {
                    rollbar.error(error);
                    reject();
                }

                let prs = JSON.parse(body);
                let rsp = extractInfo(outstanding(prs.values));
                resolve(rsp);
            });

    });
};


let outstanding = (openPrs) => {
    return openPrs.filter(x => moreThanDay(x.updatedDate));
};

let extractInfo = (arrayPrs) => {
    return arrayPrs.map(getDetails);
};

let getDetails = (pullRequest) => {
    return {
        updatedDate: pullRequest.updatedDate,
        title: pullRequest.title,
        author: pullRequest.author.user.displayName,
        reviewers: pullRequest.reviewers.map(x => x.user.emailAddress),
        url: `https://stash.clearcode.cc/${pullRequest.link.url}/overview`
    };
};

let moreThanDay = (time) => {
    return moment().diff(moment(time), 'days') > 1;
};

exports.fetchPrs = fetchPrs;
