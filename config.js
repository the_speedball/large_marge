const config = {
    production: {
        debug: false,
        json_file_store: '/opt/data'
    },
    development: {
        debug: true,
        json_file_store: './db/'
    }
};

exports.config = config;
