const config = require('./config.js').config[process.env.ENVIRONMENT || 'development'];
const debug = config.debug;

const Rollbar = require('rollbar');
const rollbar = Rollbar.init({
    accessToken: process.env.ROLLBAR_TOKEN,
    captureUncaught: true,
    captureUnhandledRejections: true,
    reportLevel: 'debug'
});


// BOT INIT
const botkit = require('botkit');
const controller = botkit.slackbot({
    debug: debug,
    json_file_store: config.json_file_store
}).configureSlackApp({
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    scopes: ['bot']
});

controller.setupWebserver(process.env.PORT, function(err, webserver) {
    controller.createWebhookEndpoints(controller.webserver);

    controller.createOauthEndpoints(controller.webserver, function(err, req, res) {
        if (err) {
            res.status(500).send('ERROR: ' + err);
        } else {
            res.send('Success!');
        }
    });
});

// just a simple way to make sure we don't
// connect to the RTM twice for the same team
let _bots = {};

function trackBot(bot) {
    _bots[bot.config.token] = bot;
}


controller.on('create_bot', function(bot, config) {

    if (_bots[bot.config.token]) {
        rollbar.info('Bot already online. Do nothing.', _bots[bot.config.token]);
        // already online! do nothing.
    } else {
        bot.startRTM(function(err) {

            if (!err) {
                trackBot(bot);
            }
            rollbar.debug(`Starting bot.`, bot);
            bot.startPrivateConversation({
                user: config.createdBy
            }, function(err, convo) {
                if (err) {
                    rollbar.error(err);
                } else {
                    convo.say('I am a bot that has just joined your team');
                    convo.say('You must now /invite me to a channel so that I can be of use!');
                }
            });

        });
    }

});


controller.storage.teams.all(function(err, teams) {

    if (err) {
        throw new Error(err);
    }

    // connect all teams with bots up to slack!
    for (var t in teams) {
        if (teams[t].bot) {
            controller.spawn(teams[t]).startRTM(function(err, bot) {
                if (err) {
                    rollbar.error('Error connecting bot to Slack:', err);
                } else {
                    rollbar.debug('Spawning bot ' + t);
                    trackBot(bot);
                }
            });
        }
    }

});

// BOT INIT END

const stashing = require('./stashing.js');
const slacking = require('./slacking.js');
const moment = require('moment');
const cron = require('cron');
const c = require('./src/message_copy.js').copies;

const CRON_TAB_DEFAULT = '00 10 * * 1-5';

let fetchSlackIds = (emails, token) => {
    return Promise.all(emails.map(email => slacking.getUsersSlackId(email, token)));
};

let makeMessage = (pr, reviewers) => {
    return new Promise((resolve, reject) => {
        resolve(`
            Title: "${pr.title}" ${pr.url}\n
            Last update: ${moment(pr.updatedDate).fromNow()}\n
            Author: ${pr.author}\n
            Reviewers: ${reviewers}`);
    }).catch(err => {
        rollbar.error(err);
    });
};


let messageMaker = (pr, token) => {
    rollbar.debug(`Making message for pr: ${pr}`);
    return fetchSlackIds(pr.reviewers, token)
        .then(slackIds => {
            return makeMessage(pr, slackIds);
        });
};

let fetchPrs = () => {
    return stashing.fetchPrs()
        .then(prs => {
            if (prs.length === 0) {
                return Promise.resolve([]);
            }
            return Promise.resolve(prs);
        });
};

// FIXME: add cron job option
// bot would allow to set a time when it runs every day

let runJobs = () => {
    rollbar.info(`Running cron jobs.`);
    controller.storage.channels.all((err, all_channels) => {
        for (let channel of all_channels) {
            rollbar.debug(`Making a cron job for ${channel.id} ${channel.daily.cron}`);
            createCheckJob(channel.daily.cron,
                channel.id);
        }
    });
};

runJobs();

let getTeamToken = (channelId) => {
    return new Promise((resolve, reject) => {
        rollbar.debug(`Fetching channel data for ${channelId}`);
        return controller.storage.channels.get(channelId, (err, channel) => {
            rollbar.debug(`Fetching team data for ${channel.team_id}`);
            return controller.storage.teams.get(channel.team_id, (err, team) => {
                rollbar.debug(`Found token for team: ${team}`);
                resolve(team.token);
            });
        });
    });
};

let makeSummary = (channelId) => {
    return Promise.all([fetchPrs(), getTeamToken(channelId)]).then(([
        prs,
        teamToken
    ]) => {
        if (prs.length === 0) {
            rollbar.debug('No outstanding PRs found.');
            return Promise.resolve([
                'Nothing to complain :unamused: This team is the best :heart:'
            ]);
        }
        return Promise.all(prs.map(pr => messageMaker(pr, teamToken)));
    });
};

let createCheckJob = (cronTime, channelId) => {
    rollbar.debug(`Creating cron job at "${cronTime}" for channel ${channelId}`);
    return new cron.CronJob({
        cronTime: cronTime,
        onTick: () => {
            rollbar.info(`Cron running for channel ID: ${channelId}.`);
            makeSummary(channelId)
                .then(messages => {
                    for (let msg of messages) {
                        getTeamToken(channelId).then(teamToken => {
                            let bot = _bots[teamToken];
                            rollbar.debug('List of available bots',
                                          Object.keys(_bots));
                            rollbar.debug(`Found channel bot.`, bot);
                            bot.say({
                                text: msg,
                                channel: channelId
                            });

                        });
                    }
                })
                .catch(err => {
                    rollbar.error(err);
                });
        },
        start: true
    });
};

controller.hears(['outstanding'], 'direct_mention,direct_message', (bot, message) => {
    rollbar.info('Heard "outstanding" message.', message);
    return makeSummary(message.channel)
        .then(messages => {
            for (let msg of messages) {
                bot.reply(message, msg);
            }
        }).catch(err => {
            rollbar.error('Sending outstanding message failed.', err);
            bot.reply(message, ':bomb: Oops! :bomb:');
        });

});


controller.hears(['help'], 'direct_mention,direct_message', (bot, message) => {
    rollbar.info('Heard "help" message.', message);
    bot.reply(message, c.help);
});

controller.on('channel_joined', (bot, message) => {
    rollbar.info('Bot joined channel', message);
    bot.say({
        text: c.greet_joined_channel,
        channel: message.channel.name
    });

    // save per channel settings
    controller.storage.channels.get(message.channel.id, (err, channel) => {

        // set defaults
        if (!channel) {
            rollbar.debug(`Channel ${message.channel.id} does not exist.`);
            channel = {
                id: message.channel.id,
                name: message.channel.name,
                team_id: bot.identifyTeam(),
                daily: {
                    cron: CRON_TAB_DEFAULT
                }
            };
            rollbar.debug(`Created ${message.channel.id}`, channel);
        }
        controller.storage.channels.save(channel);
        createCheckJob(channel.daily.cron, channel.id);
    });
});


controller.on('channel_left', (bot, message) => {
    rollbar.log('Bot left channel', message);

    controller.storage.channels.delete(message.channel, (err) => {
        rollbar.debug(`Removing channel ${message.channel} storage.`);
        if (err) {
            rollbar.error(err);
        }
    });
});
