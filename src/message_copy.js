const copies = {
    help: `
Every day at 11 I tell you a list of outstanding PRs.\n
If you wish you may ask me for *outstanding* PRs directly.\n
Just say "@large_marge outstanding".
`,
    greet_joined_channel: 'Hi guys! It is a pleasure to help you.'
};

exports.copies = copies;
